﻿using Core.Common.Library.Exceptions;
using Core.Common.Library.Interfaces;
using System;

namespace Core.Common.Library
{
    public abstract class ValidationBase<T> : IValidation where T : class
    {
        protected T Context { get; private set; }

        protected ValidationBase(string contextName, T context)
        {
            if (context == null)
            {
                throw new ArgumentNullException($"{0} is null");
            }
            Context = context;
        }

        public void Validate()
        {
            if (!IsValid)
            {
                throw new CoreException(Message);
            }
        }

        public abstract bool IsValid { get; }
        public abstract string Message { get; }
    }
}
