﻿using Core.Common.Library.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Core.Common.Library
{
    public static class Validator
    {
        public static bool CheckIfNull(string name, object context, bool throwException = true)
        {
            bool isInvalid = (context == null);
            if (isInvalid && throwException)
                throw new ArgumentException($"{name} cannot be null!");

            return isInvalid;
        }

        public static bool CheckIfValidStringLength(string name, string context, int maxLength, int minLength = 1, bool throwException = true)
        {
            bool isInvalid = (context.Length < minLength || context.Length > maxLength);
            if (isInvalid && throwException)
                throw new ArgumentException($"{name} length should be greater than {minLength} and less than {maxLength}!");

            return isInvalid;
        }

        public static bool CheckIfNullEmptyOrWhiteSpace(string name, string value, bool throwException = true)
        {
            bool isInvalid = (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value));
            if (isInvalid && throwException)
            {
                throw new ArgumentException($"The argument '{name}' must not be null, empty or whitespace.");
            }
            return isInvalid;
        }

        public static bool CheckIfNullOrEmpty<T>(string name, IEnumerable<T> value, bool throwException = true)
        {
            bool isInvalid = (null == value || !value.Any());
            if (isInvalid && throwException)
            {
                throw new ArgumentException($"The argument '{name}' must not be null or empty.");
            }
            return isInvalid;
        }

        public static bool CheckIfNotNegative(string name, int value, bool throwException = true)
        {
            bool isInvalid = (value < 0);
            if (isInvalid && throwException)
            {
                throw new ArgumentException($"The argument '{name}' must not be negative.");
            }
            return isInvalid;
        }

        public static bool CheckIfFileExists(string path, bool throwException = true)
        {
            bool isInvalid = !File.Exists(path);
            if (isInvalid && throwException) throw new ArgumentException($"File '{path}' not found!");
            return isInvalid;
        }

        public static bool CheckIfDateTimeFormatIsValid(string format, bool throwException = true)
        {
            bool isInvalid = false;
            if (format == null)
            {
                return false;
            }
            try
            {
                DateTime.Now.ToString(format, CultureInfo.InvariantCulture);
                isInvalid = true;
            }
            catch (FormatException innerException) when (throwException)
            {
                throw new ArgumentException($"The date time format is invalid.", innerException);
            }
            return isInvalid;
        }

        public static DateTime? CheckIfDateTimeIsUtc(DateTime? value, bool throwException = true)
        {
            bool isInvalid = ((value != null) && (value.Value.Kind != DateTimeKind.Utc) && (value.Value.Kind != DateTimeKind.Unspecified));
            if (isInvalid && throwException)
            {
                throw new ArgumentException($"'{value}' DateTime is not UTC");
            }
            return (value != null) ? DateTime.SpecifyKind(value.Value, DateTimeKind.Utc) : value;
        }

        public static bool CheckIfGuidIsEmpty(string name, Guid value, bool throwException = true)
        {
            bool isInvalid = (Guid.Empty == value);
            if (isInvalid && throwException)
            {
                throw new ArgumentException($"'{name}' Guid cannot be empty. value: {value}");
            }
            return isInvalid;
        }

        public static DateTime EnsureUtcTime(string name, DateTime time)
        {
            if (DateTimeKind.Unspecified == time.Kind)
            {
                throw new ArgumentException($"Unable to determine whether the supplied DateTime '{name}' is in local or UTC time.");
            }

            var converted = time;
            if (DateTimeKind.Local == time.Kind)
            {
                converted = time.ToUniversalTime();
            }
            return converted;
        }

        public static bool CheckForRange(string name, int objectToValidate, int lowerBound, int upperBound, bool throwException = true)
        {
            bool isInvalid = !Enumerable.Range(lowerBound, upperBound).Contains(objectToValidate);
            if (isInvalid && throwException)
            {
                throw new ArgumentException($"{name} is not between {lowerBound} and {upperBound}");
            }
            return isInvalid;
        }

        public static bool CheckIfBase64String(string name, string objectToValidate, bool throwException = true)
        {
            bool isInvalid = false;
            try
            {
                Convert.FromBase64String(objectToValidate);
                isInvalid = true;
            }
            catch (FormatException) when (throwException)
            {
                throw new ArgumentException($"{name} is not a valid base64 string");
            }
            return isInvalid;
        }

        public static bool CheckIfByteDataIsNullOrEmpty(string name, byte[] objectToValidate, bool throwException = true)
        {
            var isInvalid = CheckIfNull(name, objectToValidate, throwException);
            if (objectToValidate.Length == 0 && isInvalid && throwException)
            {
                throw new ArgumentException($"{name} binary data length cannot be zero");
            }
            return isInvalid;
        }

        public static bool CheckIfValidFileResource(string name, string fileResource, bool throwException = true)
        {
            CheckIfNullEmptyOrWhiteSpace(name, fileResource);
            char[] invalidCharacters = Path.GetInvalidPathChars();
            bool isInvalid = fileResource.IndexOfAny(invalidCharacters) >= 0;

            if (isInvalid && throwException)
            {
                throw new ArgumentException($"The file resource [{fileResource}] contains one or more of [{invalidCharacters}] invalid charactes.");
            }

            return isInvalid;
        }
    }
}
