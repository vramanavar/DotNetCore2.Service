﻿using System;
using System.Net;

namespace Core.Common.Library.Utilities
{
    public class Networking
    {
        public static string GetHostName()
        {
            try
            {
                return Dns.GetHostName();
            }
            catch (Exception)
            {
                return Environment.MachineName;
            }
        }
    }
}
