﻿using System.Collections.Generic;

namespace Core.Common.Library.Interfaces
{
    public interface IValidationList
    {
        bool IsValid { get; }
        bool IsAnyOneIsValid { get; }
        void Validate();
        IEnumerable<string> Messages { get; }
    }
}
