﻿using System;
using System.Runtime.Serialization;

namespace Core.Common.Library.Exceptions
{
    [Serializable]
    public class DockerException : Exception
    {
        public DockerException() { }
        public DockerException(string message) : base(message) { }
        public DockerException(string message, Exception inner) : base(message, inner) { }
        protected DockerException(SerializationInfo info,StreamingContext context): base(info, context) { }
    }
}
