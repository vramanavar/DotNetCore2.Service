﻿namespace Core.Common.Library
{
    public static class CoreConstants
    {
        public static readonly string APP_NAME = "Core.Test.Service";
        public static readonly string APP_TITLE = "Core Test Service (.Net Core 2)";
        public static readonly string APP_DESCRIPTION = ".Net Core 2 based Test Service";
        public static readonly string APP_VERSION = "1.0";
    }
}
