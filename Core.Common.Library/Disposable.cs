﻿using System;

namespace Core.Common.Library
{
    public abstract class Disposable : IDisposable
    {
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected bool Disposed { get; private set; }

        protected virtual void Dispose(bool disposing)
        {
            Disposed = true;
        }

        ~Disposable()
        {
            Dispose(false);
        }
    }
}
