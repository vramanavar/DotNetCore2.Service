﻿using System;

namespace Core.Common.Library
{
    public static class RandomExtensions
    {
        public static int GetRandomNumber(int maxLimit)
        {
            return new Random().Next(maxLimit);
        }
    }
}
