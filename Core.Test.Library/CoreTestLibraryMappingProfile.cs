﻿using AutoMapper;
using Core.Test.Library.Contracts;
using Core.Test.Repositories.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Test.Library
{
    public class CoreTestLibraryMappingProfile: Profile
    {
        public CoreTestLibraryMappingProfile()
        {
            CreateMap<DTOAccount, Account>();
            var mappingExpression = CreateMap<Account, DTOAccount>()
                .ForMember(x => x.Password, m => m.MapFrom(mx =>  "*****"));
        }
    }
}
