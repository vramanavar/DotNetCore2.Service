﻿using Core.Common.Library;
using Core.Common.Library.Exceptions;
using Core.Test.Library.Contracts;
using Core.Test.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Core.Test.Library.Validations
{
    public static class AuthenticationValidations
    {
        /// Validations pertaining to Account Authentication
        /// Note: All the validations messages could be pulled from CMS/Resource files (Hardcoded validation messages for brevity)
        /// <param name="account"></param>
        /// <returns></returns>
        public static async Task Validate(DTOAccount account, IAccountRepository accountRepository)
        {
            List<ApiError> errors = new List<ApiError>();
            if (Validator.CheckIfNull("account", account, false))
            {
                errors.Add(new ApiError("Account cannot be null or empty"));
                var nullError = new CoreException("Invalid Input", HttpStatusCode.BadRequest);
                nullError.Errors = errors;
                throw nullError;
            }

            if (Validator.CheckIfNullEmptyOrWhiteSpace("EmailId", account.EmailId, false))
            {
                errors.Add(new ApiError("Email Id cannot be null or empty", nameof(account.EmailId)));
            }

            if (Validator.CheckIfNullEmptyOrWhiteSpace("Password", account.Password, false))
            {
                errors.Add(new ApiError("Password cannot be null or empty", nameof(account.Password)));
            }

            if (errors.Count > 0)
            {
                var err = new CoreException("Invalid Input", HttpStatusCode.BadRequest);
                err.Errors = errors;
                throw err;
            }
            else
            {
                var acountFound = (await accountRepository.GetList()).Where(x => x.EmailId == account.EmailId && x.Password == account.Password).FirstOrDefault();
                if (acountFound == null) throw new CoreException("Authentication failed!", System.Net.HttpStatusCode.Unauthorized);
            }
        }
    }
}
