﻿using Core.Common.Library;
using Core.Common.Library.Exceptions;
using Core.Test.Library.Contracts;
using Core.Test.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Core.Test.Library.Validations
{
    public static class RegisterAccountValidation
    {
        /// <summary>
        /// Validations pertaining to Account Registration
        /// Note: All the validations messages could be pulled from CMS/Resource files (Hardcoded validation messages for brevity)
        /// </summary>
        /// <param name="account"></param>
        /// <param name="accountRepository"></param>
        /// <returns></returns>
        public static async Task Validate(DTOAccount account, IAccountRepository accountRepository)
        {
            List<ApiError> errors = new List<ApiError>();
            if (Validator.CheckIfNull("account", account, false))
            {
                errors.Add(new ApiError("Account cannot be null or empty"));
                var nullError = new CoreException("Invalid Input", HttpStatusCode.BadRequest);
                nullError.Errors = errors;
                throw nullError;
            }

            if (Validator.CheckIfNullEmptyOrWhiteSpace("EmailId", account.EmailId, false))
            {
                errors.Add(new ApiError("Email Id cannot be null or empty", nameof(account.EmailId)));
            }

            if (Validator.CheckIfNullEmptyOrWhiteSpace("Password", account.Password, false))
            {
                errors.Add(new ApiError("Password cannot be null or empty", nameof(account.Password)));
            }

            if (Validator.CheckIfNullEmptyOrWhiteSpace("FirstName", account.FirstName, false))
            {
                errors.Add(new ApiError("FirstName cannot be null or empty", nameof(account.FirstName)));
            }
            if (Validator.CheckIfNullEmptyOrWhiteSpace("LastName", account.LastName, false))
            {
                errors.Add(new ApiError("LastName cannot be null or empty", nameof(account.LastName)));
            }

            var existingAccount = (await accountRepository.GetList()).Find(x => string.Compare(x.EmailId, account.EmailId, true) == 0);
            if (existingAccount != null)
            {
                errors.Add(new ApiError($"An account already exists with EmailId: {account.EmailId}"));
            }

            if (errors.Count > 0)
            {
                var err = new CoreException("Invalid Input", HttpStatusCode.BadRequest);
                err.Errors = errors;
                throw err;
            }
        }
    }
}
