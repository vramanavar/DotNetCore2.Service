﻿using Core.Test.Library.Contracts.Interfaces;
using System;
using Core.Test.Library.Contracts;
using Core.Test.Repositories.Contracts;
using AutoMapper;
using Core.Test.Repositories.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Core.Test.Library.Validations;

namespace Core.Test.Library
{
    public class AccountManager : IAccountManager
    {
        //Note: Connects to Mock (In Memory) repository
        private readonly IAccountRepository _accountRepository;

        public AccountManager(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
            Mapper.Initialize((x) => { x.AddProfile<CoreTestLibraryMappingProfile>(); });
        }

        public async Task<bool> Authenticate(DTOAccount dtoAccount)
        {
            //Note: Simple Authentication for brevity - Not for Production
            await AuthenticationValidations.Validate(dtoAccount, _accountRepository);
            var accountFound = (await _accountRepository.GetList()).Count(x => x.EmailId == dtoAccount.EmailId && x.Password == dtoAccount.Password);
            return (accountFound == 1);
        }

        public async Task Register(DTOAccount dtoAccount)
        {
            await RegisterAccountValidation.Validate(dtoAccount, _accountRepository);
            await _accountRepository.Add(Mapper.Map<Account>(dtoAccount));
        }

        public async Task<bool> DeleteAccount(Guid id)
        {
            return await _accountRepository.Delete(id);
        }

        public async Task<List<DTOAccount>> GetAccounts()
        {
            var accountList = await _accountRepository.GetList();
            return Mapper.Map<List<DTOAccount>>(accountList);
        }
    }
}
