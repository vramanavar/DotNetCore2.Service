﻿using System;
using System.Runtime.Serialization;

namespace Core.Test.Library.Contracts
{
    [DataContract]
    public class DTOAccount
    {
        [DataMember]
        public Guid Id { get; set; }

        [DataMember]
        public string EmailId { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
