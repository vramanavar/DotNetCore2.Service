﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Test.Library.Contracts.Interfaces
{
    public interface IAccountManager
    {
        Task Register(DTOAccount dtoAccount);
        Task<bool> Authenticate(DTOAccount dtoAccount);
        Task<bool> DeleteAccount(Guid id);
        Task<List<DTOAccount>> GetAccounts();
    }
}
