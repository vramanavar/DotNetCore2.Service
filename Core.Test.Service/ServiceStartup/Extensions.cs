﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Test.Service.ServiceStartup
{
    public static class Extensions
    {
        public static void ConfigureJwtAuthService(this IServiceCollection services, IConfiguration configuration)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Tokens:Key" + Guid.Empty.ToString())),

                ValidateIssuer = true,
                ValidIssuer = "Tokens:Issuer",

                ValidateAudience = true,
                ValidAudience = "Tokens:Issuer",

                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(o =>
            {
                o.RequireHttpsMetadata = false; //TODO: Should be disabled only in Dev Environment
                o.SaveToken = true;
                o.TokenValidationParameters = tokenValidationParameters;
            });
        }
    }
}
