﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Core.Test.Service.Filters;
using Core.Common.Library;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Core.Test.Library;
using Core.Test.Library.Contracts.Interfaces;
using Core.Test.Repositories;
using Core.Test.Repositories.Contracts;
using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json.Serialization;

namespace Core.Test.Service.ServiceStartup
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private ILogger _logger;
        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _configuration = configuration;
            _logger = loggerFactory.CreateLogger<Startup>();
        }

        /// <summary>
        ///ConfigureServices - Adds additional services needed by components added to the IWebHost.In particular,
        ///it configures the Kestrel server options, which lets you easily define your web host setup as part of your
        ///normal config.
        /// </summary>
        /// <param name="services"></param>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(CoreConstants.APP_NAME, new Info
                {
                    Title = CoreConstants.APP_TITLE,
                    Version = CoreConstants.APP_VERSION,
                    Description = CoreConstants.APP_DESCRIPTION,
                    TermsOfService = "None",
                    //Note: The below Author/Licensing details can be pulled from application manifest (hardcoded for brevity)
                    Contact = new Contact { Name = "Vivek Ramanavar", Email = "vramanavar@gmail.com", Url = "https://gitlab.com/vramanavar" },
                    License = new License { Name = "None", Url = "https://gitlab.com/vramanavar" }
                });

                // Set the comments path for the Swagger JSON and UI.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, $"{CoreConstants.APP_NAME}.xml");
                c.IncludeXmlComments(xmlPath);
            });

            services.ConfigureJwtAuthService(_configuration);
            services.AddCors();
            services.AddMvc(config =>
            {
                // Add XML Content Negotiation
                config.RespectBrowserAcceptHeader = true;
                config.InputFormatters.Add(new XmlSerializerInputFormatter());
                config.OutputFormatters.Add(new XmlSerializerOutputFormatter());

                // Add Global Error Handling Filter
                config.Filters.Add(typeof(GlobalExceptionFilter));
            })
            .AddJsonOptions(opts =>
            {
                // Force  Camelcase to JSON serialization
                opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            }).AddControllersAsServices(); 

            //Create your Autofac Container
            var containerBuilder = new ContainerBuilder();

            //Put the framework services into Autofac
            containerBuilder.Populate(services);

            // Register Components
            RegisterComponents(containerBuilder);

            //Build and return the Autofac collection
            var container = containerBuilder.Build();
            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{CoreConstants.APP_NAME}/swagger.json", CoreConstants.APP_TITLE);
            });

            app.UseCors(policy => policy
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials()
            );

            app.UseMvc();
        }

        private void RegisterComponents(ContainerBuilder builder)
        {
            builder.RegisterInstance(_logger).As<ILogger>().SingleInstance();

            builder.RegisterType<AccountRepository>().As<IAccountRepository>().SingleInstance();
            builder.RegisterType<AccountManager>().As<IAccountManager>().SingleInstance();
        }
    }
}
