﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Core.Test.Service.ServiceStartup;

namespace Core.Test.Service
{
    public class Program
    {
        public static int Main(string[] args)
        {
            try
            {
                var host = new WebHostBuilder()
                     .UseKestrel()
                     .UseContentRoot(Directory.GetCurrentDirectory())
                     .ConfigureAppConfiguration(ConfigConfiguration)
                     .ConfigureLogging(LoggingConfiguration)
                     .UseStartup<Startup>()
                     .Build();

                host.Run();

                return 0;
            }
            catch (Exception excp)
            {
                Console.WriteLine($"Failed to startup -> {excp.ToString()}");
                return 1;
            }
        }

        private static void ConfigConfiguration(WebHostBuilderContext ctx, IConfigurationBuilder config)
        {
            config.SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
              .AddJsonFile($"appsettings.{ctx.HostingEnvironment.EnvironmentName}.json", optional: false, reloadOnChange: true)
              .AddEnvironmentVariables();
        }

        private static void LoggingConfiguration(WebHostBuilderContext ctx, ILoggingBuilder loggingBuilder)
        {
            var serilogConfig = new LoggerConfiguration().ReadFrom.ConfigurationSection(ctx.Configuration.GetSection("Serilog"));
            loggingBuilder.AddSerilog(serilogConfig.CreateLogger());
        }
    }
}
