﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using Core.Test.Library.Contracts.Interfaces;
using Core.Test.Library.Contracts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Core.Test.Service.Filters;

namespace Core.Test.Service.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("[controller]/[action]")]
    public class SecurityController : Controller
    {
        private readonly IConfiguration _config;
        private readonly IAccountManager _accountManager;

        public SecurityController(IConfiguration config, IAccountManager accountManager)
        {
            _config = config;
            _accountManager = accountManager;
        }

        /// <summary>
        /// Generates Auth Token for given credentials (Authenticates user before generating token)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> GenerateAuthToken([FromBody] DTOAccount dtoAccount)
        {
            if (await _accountManager.Authenticate(dtoAccount))
            {
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, dtoAccount.EmailId),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Tokens:Key" + Guid.Empty.ToString()));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken("Tokens:Issuer",
                    "Tokens:Issuer",
                    claims,
                    expires: DateTime.Now.AddDays(30),
                    signingCredentials: creds);

                return Ok(new { token = new JwtSecurityTokenHandler().WriteToken(token) });
            }

            return BadRequest("Could not create token");
        }

        /// <summary>
        /// Register New User by providing EmailId, Password, FirstName and LastName
        /// </summary>
        /// <param name="dtoAccount"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task Register([FromBody] [Bind("EmailId,FirstName,LastName,Password")]  DTOAccount dtoAccount)
        {
            await _accountManager.Register(dtoAccount);
        }

        /// <summary>
        /// Authenticate the user by EmailId and Password
        /// </summary>
        /// <param name="dtoAccount"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Authenticate([FromBody] [Bind("EmailId,Password")] DTOAccount dtoAccount)
        {
            return Ok(await _accountManager.Authenticate(dtoAccount));
        }

        /// <summary>
        /// Delete a specific Account by providing an account id (Requires an autenticated user)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        public async Task<IActionResult> DeleteAccount([FromQuery] Guid id)
        {
            return Ok(await _accountManager.DeleteAccount(id));
        }

        /// <summary>
        /// List all the accounts in the system (Provide [Accept: text/xml] header for content negotiation)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAccounts()
        {
            return Ok(await _accountManager.GetAccounts());
        }
    }
}
