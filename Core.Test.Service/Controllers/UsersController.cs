﻿using Bogus;
using Core.Test.Library.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Common.Library;
using Core.Test.Service.Filters;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Core.Test.Service.Controllers
{
    public enum Gender
    {
        Male,
        Female
    }

    public enum AddressType
    {
        Home,
        Office,
        Remote
    }

    [Produces("application/json")]
    [Route("[controller]/[action]")]
    [CustomFilter(new object[] { CustomFlag.FlagA, CustomFlag.FlagB, CustomFlag.FlagC})]
    public class UsersController : Controller
    {
        /// <summary>
        /// List all the accounts in the system (Provide [Accept: text/xml] header for content negotiation)
        /// </summary>
        /// <returns></returns>
        /// 
        private static readonly List<DTOUser> UsersList;
        private readonly IHostingEnvironment _hostingEnvironment;
        //Note: Be wary of static variables in concurrency hits; This acts as Fake data only
        static UsersController()
        {
            UsersList = GenerateDummyUserData();
        }

        public UsersController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Returns a list of Fake Users (By default 100 rows)
        /// </summary>
        /// <returns>List of DTOUser</returns>
        [HttpGet]
        public async Task<IActionResult> GetUsers(string filter = "", string sortDirection = "asc", int? pageNumber = null, int? pageSize = null)
        {
            List<DTOUser> result = UsersList;

            if (pageNumber.HasValue && pageSize.HasValue && pageSize.Value > 0)
            {
                result = UsersList.Skip(pageNumber.Value * pageSize.Value).Take(pageSize.Value).ToList();
            }

            switch (sortDirection.ToLower())
            {
                case "asc":
                    result = result.OrderBy(x => x.FirstName).ToList();
                    break;
                default:
                    result = result.OrderByDescending(x => x.FirstName).ToList();
                    break;
            }

            if (!string.IsNullOrWhiteSpace(filter))
            {
                result = result.FindAll(x => x.FirstName.Contains(filter) ||
                                             x.LastName.Contains(filter) ||
                                             x.Email.Contains(filter) ||
                                             x.AddressType.Contains(filter) ||
                                             x.Street1.Contains(filter) ||
                                             x.Street2.Contains(filter) ||
                                             x.City.Contains(filter) ||
                                             x.State.Contains(filter) ||
                                             x.Zip.Contains(filter)
                                             ).ToList();
            }
            
            return Ok(await Task.FromResult(result));
        }

        /// <summary>
        /// Returns Users List as CSV Download
        /// </summary>
        /// <returns>List of Users as CSV</returns>
        [HttpGet]
        public async Task GetUsersAsCsv()
        {
            var filename = "Users";

            var webRootPath = _hostingEnvironment.ContentRootPath + @"\Log.txt";
            //Note: Quick string write operation to file. ***Typically not be treated as logging infrastructure***
            "Begining to write File".WriteToFile(webRootPath);

            ControllerContext.HttpContext.Response.Clear();
            ControllerContext.HttpContext.Response.Headers.Add(new KeyValuePair<string, StringValues>("content-disposition", string.Format("attachment; filename={0}.csv", filename)));
            ControllerContext.HttpContext.Response.Headers.Add(new KeyValuePair<string, StringValues>("Pragma", "public"));
            ControllerContext.HttpContext.Response.ContentType = "text/csv";

            "Completed writing".WriteToFile(webRootPath);

            await ControllerContext.HttpContext.Response.WriteAsync(UsersList.GetCsv());
        }

        private static List<DTOUser> GenerateDummyUserData(int userCount = 100)
        {
            Faker faker = new Faker();
            var fakeUserList = new List<DTOUser>();
            for (var index = 0; index < userCount; index++)
            {
                var user = new Person();
                fakeUserList.Add(new DTOUser()
                {
                    Id = index,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Gender = (int)faker.PickRandom<Gender>(),
                    Email = user.Email,
                    Password = "",
                    AddressType = faker.PickRandom<AddressType>().ToString(),
                    Street1 = user.Address.Suite,
                    Street2 = user.Address.Street,
                    City = user.Address.City,
                    State = "",
                    Zip = user.Address.ZipCode
                });
            }

            return fakeUserList;
        }
    }
}