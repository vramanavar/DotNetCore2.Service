﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace Core.Test.Service.Filters
{
    public enum CustomFlag {
        FlagA,
        FlagB,
        FlagC,
        FlagD
    };

    public class CustomFilter: ActionFilterAttribute
    {
        private readonly CustomFlag[] customFlags;
        public CustomFilter(params object[] flags)
        {
            if (flags.Any(r => r.GetType() != typeof(CustomFlag))) throw new ApplicationException("Invalid flags");
            customFlags = flags.Select(x => (CustomFlag) x).ToArray();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Validate and handle accordingly
            foreach (var flag in customFlags) {
            }            
        }
    }
}
