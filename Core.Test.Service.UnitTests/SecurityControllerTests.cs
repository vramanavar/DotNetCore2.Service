﻿using Core.Test.Library.Contracts;
using Core.Test.Library.Contracts.Interfaces;
using Core.Test.Service.Controllers;
using Microsoft.Extensions.Configuration;
using NSubstitute;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Core.Test.Service.UnitTests
{
    public class SecurityControllerTests
    {

        [Fact]
        public async Task When_GenerateTokenIsCalled_WithValidCredentials_TokenShouldBe_ReturnedSuccessfully()
        {
            //Arrange
            var config = Substitute.For<IConfiguration>();
            var accountManager = Substitute.For<IAccountManager>();
            accountManager.Authenticate(Arg.Any<DTOAccount>()).Returns(true);
            var securityController = new SecurityController(config, accountManager);

            //Act
            var dtoAccount = new DTOAccount() { EmailId = "test@test.com", Password = "test" };
            var result = await securityController.GenerateAuthToken(dtoAccount);

            //Assert
            Assert.NotNull(result);
            Assert.True(result is Microsoft.AspNetCore.Mvc.OkObjectResult);
        }

        [Fact]
        public async Task When_GenerateTokenIsCalled_WithInvalidCredentials_TokenShould_NotBe_Returned()
        {
            //Arrange
            var config = Substitute.For<IConfiguration>();
            var accountManager = Substitute.For<IAccountManager>();
            accountManager.Authenticate(Arg.Any<DTOAccount>()).Returns(false);
            var securityController = new SecurityController(config, accountManager);

            //Act
            var dtoAccount = new DTOAccount() { EmailId = "test@test.com", Password = "test" };
            var result = await securityController.GenerateAuthToken(dtoAccount);

            //Assert
            Assert.True(result is Microsoft.AspNetCore.Mvc.BadRequestObjectResult);
        }
    }
}
