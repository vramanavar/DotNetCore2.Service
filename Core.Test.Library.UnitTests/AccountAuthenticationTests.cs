﻿using Core.Common.Library.Exceptions;
using Core.Test.Library.Contracts;
using Core.Test.Library.Contracts.Interfaces;
using Core.Test.Repositories;
using Core.Test.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using System.Linq;

namespace Core.Test.Library.UnitTests
{

    public class AccountAuthenticationTests
    {
        [Fact]
        public async Task When_ValidCredentialsAreProvided_Then_LoginShould_Succeed()
        {
            //Arrange
            IAccountManager accountManager = GetAccountManager();

            //Act
            DTOAccount newAccount = GetDTOAccountObject("test@test.com", "test", "FirstName", "LastName");
            await accountManager.Register(newAccount);

            DTOAccount credentials = new DTOAccount() { EmailId = "test@test.com", Password = "test" };
            var success = await accountManager.Authenticate(credentials);

            //Assert
            Assert.True(success);
        }

        private DTOAccount GetDTOAccountObject(string emailId, string password, string firstName = "", string lastName = "")
        {
            DTOAccount newAccount = new DTOAccount();
            newAccount.EmailId = emailId;
            newAccount.Password = password;
            newAccount.FirstName = firstName;
            newAccount.LastName = lastName;
            return newAccount;
        }

        private static IAccountManager GetAccountManager()
        {
            IAccountRepository accountRepository = new AccountRepository();
            IAccountManager accountManager = new AccountManager(accountRepository);
            return accountManager;
        }

        [Theory]
        [InlineData("test@test.com", "")]
        [InlineData("test@test.com", "Test")]
        public async Task When_InvalidCredentialsAreProvided_Then_LoginShould_Fail(string emailId, string password)
        {

            //Arrange
            IAccountManager accountManager = GetAccountManager();

            //Act
            DTOAccount newAccount = GetDTOAccountObject(emailId, "test", "FirstName", "LastName");
            await accountManager.Register(newAccount);

            DTOAccount credentials = new DTOAccount() { EmailId = emailId, Password = password };

            var exception = await Record.ExceptionAsync(async () =>
            {
                await accountManager.Authenticate(credentials);
            });

            //Assert
            if (string.IsNullOrWhiteSpace(password))
            {
                Assert.Equal(HttpStatusCode.BadRequest, ((CoreException)exception).StatusCode);
            }
            else
            {
                Assert.Equal(HttpStatusCode.Unauthorized, ((CoreException)exception).StatusCode);
            }
        }

    }
}
