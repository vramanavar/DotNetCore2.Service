﻿using Core.Test.Repositories.Contracts;
using System;
using Xunit;
using Core.Test.Repositories;
using Core.Test.Library.Contracts.Interfaces;
using Core.Test.Library.Contracts;
using System.Threading.Tasks;
using System.Linq;
using Core.Common.Library.Exceptions;
using System.Net;

namespace Core.Test.Library.UnitTests
{
    public class RegisterAccountTests
    {
        [Fact]
        public async Task When_AllDataIsProvided_AccountShouldGetRegistered_Successfully()
        {
            //Arrange
            IAccountManager accountManager = GetAccountManager();

            //Act
            DTOAccount newAccount = GetDTOAccountObject("test@test.com", "test", "FirstName", "LastName");
            await accountManager.Register(newAccount);

            //Assert
            var registeredNewAccount = (await accountManager.GetAccounts()).Where(x => x.EmailId == newAccount.EmailId).FirstOrDefault();
            Assert.NotNull(registeredNewAccount);
            Assert.Equal(newAccount.EmailId, registeredNewAccount.EmailId);
            Assert.Equal(newAccount.FirstName, registeredNewAccount.FirstName);
            Assert.Equal(newAccount.LastName, registeredNewAccount.LastName);
            Assert.IsType<Guid>(registeredNewAccount.Id);
            Assert.NotEqual(Guid.Empty, registeredNewAccount.Id);
        }

        private DTOAccount GetDTOAccountObject(string emailId, string password, string firstName, string lastName)
        {
            DTOAccount newAccount = new DTOAccount();
            newAccount.EmailId = emailId;
            newAccount.Password = password;
            newAccount.FirstName = firstName;
            newAccount.LastName = lastName;
            return newAccount;
        }

        private static IAccountManager GetAccountManager()
        {
            IAccountRepository accountRepository = new AccountRepository();
            IAccountManager accountManager = new AccountManager(accountRepository);
            return accountManager;
        }

        [Theory]
        [InlineData("","","","")]
        [InlineData("test@test.com", "", "", "")]
        [InlineData("test@test.com", "test", "", "")]
        [InlineData("test@test.com", "test", "firstName", "")]
        [InlineData("", "test", "firstName", "lastName")]
        [InlineData(null, null, null, null)]
        [InlineData("test@test.com", "test", null, null)]
        [InlineData(null, null, "firstName", "lastName")]
        public async Task When_InvalidDataIsProvided_AccountRegistrationShould_Fail(string emailId, string password, string firstName, string lastName)
        {
            //Arrange
            IAccountManager accountManager = GetAccountManager();

            //Act
            DTOAccount newAccount = GetDTOAccountObject(emailId, password, firstName, lastName);
            var exception = await Record.ExceptionAsync( async () =>
            {
                await accountManager.Register(newAccount);
            });


            //Assert
            Assert.Equal(HttpStatusCode.BadRequest, ((CoreException)exception).StatusCode);
        }

    }
}
