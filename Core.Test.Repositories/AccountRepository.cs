﻿using Core.Test.Repositories.Contracts;
using System;
using Core.Test.Repositories.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Test.Repositories
{
    /// <summary>
    /// This repositories is responsible for persistence of Account Information
    /// Note: Essentialy Database Context would interact with backend database to persist and retrieve information
    /// Using In-Memory List of Account objects for brevity
    /// </summary>
    public class AccountRepository : IAccountRepository
    {
        //In-Memory Account table
        private List<Account> _accountList = new List<Account>()
        {
            new Account()
            {
                Id = Guid.NewGuid(),
                EmailId = "vramanavar@test.com",
                Password = "test",
                FirstName = "Vivek",
                LastName = "Ramanavar"
            },
            new Account()
            {
                Id = Guid.NewGuid(),
                EmailId = "giridhar@test.com",
                Password = "test",
                FirstName = "Giridhar",
                LastName = "Kalmadka"
            },
            new Account()
            {
                Id = Guid.NewGuid(),
                EmailId = "kumara@test.com",
                Password = "test",
                FirstName = "Kumaresh",
                LastName = "Settappa"
            },
            new Account()
            {
                Id = Guid.NewGuid(),
                EmailId = "nishanth@test.com",
                Password = "test",
                FirstName = "Nishanth",
                LastName = "Marathe"
            },
            new Account()
            {
                Id = Guid.NewGuid(),
                EmailId = "system@test.com",
                Password = "test",
                FirstName = "System",
                LastName = "Admin"
            }
        };

        public Task Add(Account account)
        {
            account.Id = Guid.NewGuid();
            _accountList.Add(account);
            return Task.FromResult<List<Account>>(null);
        }

        public Task<bool> Delete(Guid Id)
        {
            bool success = true;
            try
            {
                _accountList.RemoveAll(x => x.Id == Id);
            }
            catch(Exception ex)
            {
                //Log the exception here
                success = false;
            }
            return Task.FromResult(success);
        }

        public Task<List<Account>> GetList()
        {
            return Task.FromResult(_accountList);
        }
    }
}
