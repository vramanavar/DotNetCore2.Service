﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Test.Repositories.Models
{
    public class Account
    {
        public Guid Id { get; set; }

        public string EmailId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Password { get; set; }
    }
}
