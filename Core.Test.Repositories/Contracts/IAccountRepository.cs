﻿using Core.Test.Repositories.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Test.Repositories.Contracts
{
    public interface IAccountRepository
    {
        Task <List<Account>> GetList();
        Task Add(Account account);
        Task<bool> Delete(Guid Id);
    }
}
