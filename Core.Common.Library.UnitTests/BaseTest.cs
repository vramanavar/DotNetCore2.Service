﻿using Core.Common.Library;
using Xunit.Abstractions;

namespace Core.Common.UnitTests
{
    public abstract class BaseTest : Disposable
    {
        protected readonly ITestOutputHelper xunitOutput;

        protected BaseTest(ITestOutputHelper output)
        {
            this.xunitOutput = output;
            //Global Setup implementation
        }
    }
}
