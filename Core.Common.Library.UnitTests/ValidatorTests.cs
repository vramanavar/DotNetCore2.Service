﻿using Core.Common.Library;
using FsCheck.Xunit;
using System;
using Xunit;

namespace Core.Common.UnitTests
{
    public class ValidatorTests
    {
        [Property]
        public void When_ValidObjectIsPassed_Then_ItShouldNotThrowException(object obj)
        {
            if (obj != null)
            {
                var excp = Record.Exception(() =>
                {
                    Validator.CheckIfNull("obj", obj);
                });

                Assert.Null(excp);
            }
        }

        [Property]
        public void When_NullObjectIsPassed_Then_ItShouldThrowException(object obj)
        {
            if (obj == null)
            {
                var excp = Record.Exception(() =>
                {
                    Validator.CheckIfNull("obj", obj);
                });

                Assert.NotNull(excp);
            }
        }

        [Property]
        public void When_IntegerValueIsWithinRange_Then_ItShouldNotThrowException(int value)
        {
            if (value >= 10 && value <= 100)
            {
                var excp = Record.Exception(() =>
                {
                    Validator.CheckForRange("Value", value, 10, 100);
                });

                Assert.Null(excp);
            }
        }

        [Property]
        public void When_IntegerValueIsOutOfRange_Then_ItShouldThrowException(int value)
        {
            if (value < 10 && value > 100)
            {
                var excp = Record.Exception(() =>
                {
                    Validator.CheckForRange("Value", value, 10, 100);
                });

                Assert.NotNull(excp);
            }
        }

        [Property]
        public void When_UtcTimeIsPassed_Then_ItShouldSucceed(DateTime time)
        {
            if (time.Kind == DateTimeKind.Utc)
            {
                var exception = Record.Exception(() =>
                {
                    Validator.EnsureUtcTime("t1", time);
                });

                Assert.Null(exception);
            }
        }

        [Property]
        public void When_NonUtcTimeIsPassed_Then_ItShouldFail(DateTime time)
        {
            if (time.Kind != DateTimeKind.Utc)
            {
                var exception = Record.Exception(() =>
                {
                    Validator.EnsureUtcTime("t1", time);
                });

                Assert.NotNull(exception);
            }
        }

        [Fact]
        public void When_EmptyGuidIsPassed_Then_ItShouldFail()
        {
            var exception = Record.Exception(() =>
            {
                Validator.CheckIfGuidIsEmpty("GuidValue", Guid.Empty);
            });

            Assert.NotNull(exception);
        }

        [Property]
        public void When_NonEmptyGuidIsPassed_Then_ItShouldSucceed(Guid guid)
        {
            if (guid != Guid.Empty)
            {
                var exception = Record.Exception(() =>
                {
                    Validator.CheckIfGuidIsEmpty("GuidValue", guid);
                });

                Assert.Null(exception);
            }
        }

        [Property]
        public void When_NegativeNumberIsPassed_Then_ItShouldFail(int value)
        {
            if (value < 0)
            {
                var exception = Record.Exception(() =>
                {
                    Validator.CheckIfNotNegative("value", value);
                });
                Assert.NotNull(exception);
            }
        }

        [Property]
        public void When_PositiveNumberIsPassed_Then_ItShouldNotThrowException(int value)
        {
            if (value >= 0)
            {
                var exception = Record.Exception(() =>
                {
                    Validator.CheckIfNotNegative("value", value);
                });
                Assert.Null(exception);
            }
        }

        [Property]
        public void When_NonUTCDateTimeIsPassed_Then_ItShouldThrowException(DateTime dateTime)
        {
            if (dateTime.Kind != DateTimeKind.Utc && dateTime.Kind != DateTimeKind.Unspecified)
            {
                var exception = Record.Exception(() =>
                {
                    Validator.CheckIfDateTimeIsUtc(dateTime);
                });
                Assert.NotNull(exception);
            }
        }

        [Property]
        public void When_UTCDateTimeIsPassed_Then_ItShouldNOTThrowException(DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Utc)
            {
                var exception = Record.Exception(() =>
                {
                    Validator.CheckIfDateTimeIsUtc(dateTime);
                });
                Assert.Null(exception);
            }
        }

        [Theory]
        [InlineData("h:mm tt")]
        [InlineData("M/d/yyyy")]
        [InlineData("h:mm:ss tt")]
        [InlineData("dddd, MMMM dd, yyyy")]
        [InlineData("dddd, MMMM dd, yyyy h:mm tt")]
        public void When_ValidDateTimeFormatIsPassed_Then_ItShouldNOTThrowException(string format)
        {
            if (!string.IsNullOrEmpty(format))
            {
                var exception = Record.Exception(() =>
                {
                    Validator.CheckIfDateTimeFormatIsValid(format);
                });
                Assert.Null(exception);
            }
        }

    }
}
