﻿using Core.Common.Library;

namespace Core.Common.UnitTests.CodeSamples
{
    public class Age18YearsOrHigherValidation : ValidationBase<Person>
    {
        private const short MinDrivingAge = 18;

        public Age18YearsOrHigherValidation(Person context) : base("Age18YearsOrHigherValidation", context) { }

        public override bool IsValid
        {
            get { return Context.Age >= MinDrivingAge; }
        }

        public override string Message
        {
            get
            {
                return $"{Context.Name}'s age {0} is less than minimum driving age of {MinDrivingAge}";
            }
        }
    }
}
