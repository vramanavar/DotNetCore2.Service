﻿namespace Core.Common.UnitTests.CodeSamples
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public bool CanDrive { get; set; }
    }
}
