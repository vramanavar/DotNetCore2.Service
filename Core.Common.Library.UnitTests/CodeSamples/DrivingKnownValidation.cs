﻿using Core.Common.Library;

namespace Core.Common.UnitTests.CodeSamples
{
    public class DrivingKnownValidation : ValidationBase<Person>
    {
        public DrivingKnownValidation(Person context) : base("DrivingKnownValidation", context)
        {

        }

        public override bool IsValid
        {
            get { return Context.CanDrive; }
        }

        public override string Message
        {
            get
            {
                return $"{Context.Name} cannot drive.";
            }
        }
    }
}
