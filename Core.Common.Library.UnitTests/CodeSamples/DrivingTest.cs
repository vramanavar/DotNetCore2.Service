﻿using Core.Common.Library.Interfaces;
using System.Collections.Generic;

namespace Core.Common.UnitTests.CodeSamples
{
    public class DrivingTest
    {
        private readonly IValidationList _validationList;

        public DrivingTest(IValidationList validationList)
        {
            _validationList = validationList;
        }

        private bool IsValid
        {
            get
            {
                return _validationList.IsValid;
            }
        }

        public IEnumerable<string> Messages
        {
            get
            {
                return _validationList.Messages;
            }
        }
    }
}
