﻿using Core.Common.Library;
using Core.Common.UnitTests.CodeSamples;
using FsCheck.Xunit;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace Core.Common.UnitTests
{
    public class BusinessRuleValidationTests : BaseTest
    {
        public BusinessRuleValidationTests(ITestOutputHelper output) : base(output)
        {
        }

        [Property]
        public void When_DrivingIsKnown_Then_ItShouldSucceedElseFail(bool drivingKnown)
        {
            DrivingKnownValidationHelper(16, drivingKnown, expected: drivingKnown);
        }

        [Property]
        public void When_AgeIs18OrAbove_Then_ItShouldSucceed(int age)
        {
            if (age >= 18)
            {
                DrivingKnownValidationHelper(age, true, expected: true);
            }
        }

        [Property]
        public void When_AgeIsLessThan18_Then_ItShouldFail(int age)
        {
            if (age < 18)
            {
                Age18OrAboveValidationHelper(age, true, expected: false);
            }
        }

        [Property]
        public void When_AgeAndDrivingIsNotMeetingCondition_Then_ItShouldFail(int age, bool canDrive)
        {
            if (age < 18 && !canDrive)
            {
                Both_Age18OrAbove_And_DrivingKnownValidationHelper(age, canDrive, expected: false);
            }
        }

        [Property]
        public void When_AgeAndDrivingIsNotMeetingCondition_Then_ValidateIfErrorMessagesAreReturned(int age, bool canDrive)
        {
            if (age < 18 && !canDrive)
            {
                var errorMessages = Both_Age18OrAbove_And_DrivingKnownValidationHelper(age, canDrive, expected: false);
                Assert.Equal(2, errorMessages.Count());
            }
        }

        [Property]
        public void When_AgeAndDrivingIsNotMeetingCondition_Then_CoreExceptionShouldBeThrown(int age, bool canDrive)
        {
            if (age < 18 && !canDrive)
            {
                var person = new Person { Age = age, CanDrive = canDrive };
                var age18YearsOrAboveValidation = new Age18YearsOrHigherValidation(person);
                var drivingKnownValidation = new DrivingKnownValidation(person);

                ValidationList validationList = new ValidationList();
                validationList.Add(age18YearsOrAboveValidation);
                validationList.Add(drivingKnownValidation);

                var exception = Record.Exception(() => {
                    validationList.Validate();
                }
                );
                Assert.NotNull(exception);
                Assert.Equal<bool>(false, validationList.IsValid);
                Assert.Equal(2, validationList.Messages.Count());
            }
        }

        [Property]
        public void When_AnyOneConditionIsValid_Then_ItShouldSucceed(int age, bool canDrive)
        {
            if ((age < 18 && canDrive) || (age >= 18 && !canDrive))
            {
                var person = new Person { Age = age, CanDrive = canDrive };
                var age18YearsOrAboveValidation = new Age18YearsOrHigherValidation(person);
                var drivingKnownValidation = new DrivingKnownValidation(person);

                ValidationList validationList = new ValidationList();
                validationList.Add(age18YearsOrAboveValidation);
                validationList.Add(drivingKnownValidation);

                var exception = Record.Exception(() => {
                    validationList.Validate();
                }
                );
                Assert.NotNull(exception);
                Assert.Equal(1, validationList.Messages.Count());
                Assert.Equal<bool>(true, validationList.IsAnyOneIsValid);
            }
        }

        [Property]
        public void When_BothAgeAndDrivingIsMeetingCondition_Then_ItShouldSucceed(int age, bool canDrive)
        {
            if (age >= 18 && canDrive)
            {
                Both_Age18OrAbove_And_DrivingKnownValidationHelper(age, canDrive, expected: true);
            }
        }

        private IEnumerable<string> Both_Age18OrAbove_And_DrivingKnownValidationHelper(int age, bool canDrive, bool expected)
        {
            var person = new Person { Age = age, CanDrive = canDrive };
            var age18YearsOrAboveValidation = new Age18YearsOrHigherValidation(person);
            var drivingKnownValidation = new DrivingKnownValidation(person);
            ValidationList validationList = new ValidationList();
            validationList.Add(age18YearsOrAboveValidation);
            validationList.Add(drivingKnownValidation);

            var exception = Record.Exception(() => {
                validationList.Validate();
            }
            );
            Assert.Equal<bool>(expected, validationList.IsValid);
            return validationList.Messages;
        }

        private void Age18OrAboveValidationHelper(int age, bool canDrive, bool expected)
        {
            var person = new Person { Age = age, CanDrive = canDrive };
            var age18YearsOrAboveValidation = new Age18YearsOrHigherValidation(person);
            Assert.Equal<bool>(expected, age18YearsOrAboveValidation.IsValid);
        }

        private void DrivingKnownValidationHelper(int age, bool canDrive, bool expected)
        {
            var person = new Person { Age = age, CanDrive = canDrive };
            var drivingKnownValidation = new DrivingKnownValidation(person);
            Assert.Equal<bool>(expected, drivingKnownValidation.IsValid);
        }
    }
}
